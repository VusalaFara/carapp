package az.ingress.carapp;

import az.ingress.carapp.config.Properities;
import az.ingress.carapp.model.Car;
import az.ingress.carapp.service.CarService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class CarappApplication implements CommandLineRunner {

	private final Properities properities;
	private final CarService service;


	public static void main(String[] args) {
		SpringApplication.run(CarappApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		for (int a = 0; a < properities.getModel().size(); a++) {
			service.saveCar(new Car(a+1, properities.getName().get(a),
					properities.getModel().get(a),
					properities.getEngineType().get(a)));
		}

//		for (String p : properities.getModel())
//			service.saveCar(new Car(1,"aa",p,"engintype"));
//
//
//		}

//		for (int a = 0; a < properities.getModel().size(); a++) {
//			service.saveCar(new Car(a+1, "aa", properities.getModel().get(a), "engineType"));
//		}
////	}
//

	}
}


