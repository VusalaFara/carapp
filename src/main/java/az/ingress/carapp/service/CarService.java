package az.ingress.carapp.service;

import az.ingress.carapp.model.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {

    public Optional<Car> findById(Integer id);
    public List<Car> getCar();
    public String saveCar(Car car);
    public String updateCar(Integer id, Car car);
    public Integer deleteCar(Integer id);

}
