package az.ingress.carapp.service.impl;

import az.ingress.carapp.model.Car;
import az.ingress.carapp.repository.CarRepository;
import az.ingress.carapp.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository repository;

    @Override
    public Optional<Car> findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public List<Car> getCar() {
        return repository.findAll();
    }

    @Override
    public String saveCar(Car car) {
        repository.save(car);
        return "added";
    }
//        public String updateCar(Integer id, Car car){
//       Optional<Car> car1 = repository.findById(id);
//       car1.get().setName(car.getName());
//       car1.get().setModel(car.getModel());
//       car1.get().setEngineType(car.getEngineType());
//       repository.save(car1.get());
//       return "added";
//
//   }



    public String updateCar(Integer id, Car car){
          repository.findById(id).stream().map(car1 -> {
             car1.setName(car.getName());
             car1.setModel(car.getModel());
             car1.setEngineType(car.getEngineType());
             return repository.save(car1);
         }).findAny().get();
          return "added";
    }

//        public String updateCar(Integer id, Car car){
//          if(repository.findById(id).isPresent()){
//              Car car1 = repository.findById(id).get();
//                  car1.setName(car.getName());
//                  car1.setModel(car.getModel());
//                  car1.setEngineType(car.getEngineType());
//                  repository.save(car1);
//              }else {
//              throw new RuntimeException("user not found with : "+ id);
//          }
//          return "added";
//    }


    @Override
    public Integer deleteCar(Integer id) {
        repository.deleteById(id);
        return id;
    }
}
