package az.ingress.carapp.controller;

import az.ingress.carapp.model.Car;
import az.ingress.carapp.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController {

    private final CarService service;

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable Integer id){

        return service.findById(id).get();
    }

    @GetMapping
    public List<Car> getCar(){
        return service.getCar();

    }

    @PostMapping
    public String saveCar(@RequestBody Car car){
        service.saveCar(car);
        return "added car with name: "+car.getName();
    }
    @PutMapping ("/{id}")
    public String updateCar(@PathVariable Integer id,@RequestBody Car car){
        service.updateCar(id, car);
        return "updated car with id: "+id;
    }

    @DeleteMapping("/{id}")
    public String deleteCar(@PathVariable Integer id){
        service.deleteCar(id);
        return "deleted car with id: "+id;
    }
}



