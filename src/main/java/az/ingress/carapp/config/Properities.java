package az.ingress.carapp.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("car")
@Data
public class Properities {

    List <String> name;

    List <String> model;

    List <String> engineType;

}
